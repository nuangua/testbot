# Run Command


## Allow powershell execution permission

```buildoutcfg
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope LocalMachine -Force
```

## Run from local

```buildoutcfg
powershell -f .\deploy.ps1 --Action Install --Name template
```
## Run from cloud

```buildoutcfg
# On Windows
powershell -command "Invoke-Command -ScriptBlock ([scriptblock]::Create($((new-object net.webclient).DownloadString('https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.ps1')))) -ArgumentList 'Install','template'"

# On Linux
curl -s https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.sh | bash -s Install template
pwsh -command 'Invoke-Command -ScriptBlock ([scriptblock]::Create($((new-object net.webclient).DownloadString("https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.ps1")))) -ArgumentList "Install","template"'
```
# APIs

## Install testbot

* For Windows

```buildoutcfg
powershell -command "Invoke-Command -ScriptBlock ([scriptblock]::Create($((new-object net.webclient).DownloadString('https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.ps1')))) -ArgumentList 'Install','testbot'"
```

* For Linux/Unix

```buildoutcfg
curl -s https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.sh | bash -s Install testbot
```