#!/bin/bash

export Action=$1
export Name=$2
echo 'Action=' $Action
echo 'Name=' $Name

# install powershell core

if [[ ! -x "$(command -v pwsh)" ]]; then
	echo 'pwsh is not installed.'
	install_dir="/opt/powershell"
	download_filename="powershell-7.4.2-linux-arm64.tar.gz"
	if [[ ! -f "/opt/$download_filename" ]]; then
	  echo "downloading file $download_filename to directory /opt/$download_filename..."
    curl -o /opt/$download_filename https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/libraries/powershell/$download_filename
	fi
	if [[ ! -e "$install_dir" ]]; then
		mkdir -p /opt/powershell
	elif [[ ! -d "$install_dir" ]]; then
		echo "$install_dir already exists but is not a directory!"
	fi

	if [[ ! -f "/opt/powershell/pwsh" ]]; then
		tar zxvf /opt/powershell-7.4.2-linux-arm64.tar.gz -C /opt/powershell
		chmod +x -R /opt/powershell
	fi
	if [[ ! -f "/usr/bin/pwsh" ]]; then
	  ln -s /opt/powershell/pwsh /usr/bin/pwsh
	fi

fi

pwsh -V

pwsh -command 'Invoke-Command -ScriptBlock ([scriptblock]::Create($((new-object net.webclient).DownloadString("https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/deploy.ps1")))) -ArgumentList '$Action','$Name