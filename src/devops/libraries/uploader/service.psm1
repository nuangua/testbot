#######################################################################################
########## License:Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved           ##########
########## Author : Nuanguang Gu(Sunny)                                      ##########
########## Email  : nuanguang.gu@aliyun.com                                  ##########
#######################################################################################


#######################################################################################
# 用法:
# * 设置ROOT_PATH变量
# set ROOT_PATH=D:\codes\tbot\dist\html
# * 设置NEXUS_REPO_PATH变量
# set NEXUS_REPO_PATH=docs/tbot
# * 运行powershell命令，上传ROOT_PATH的所有文件到Nexus的目录NEXUS_REPO_PATH
# powershell -f .\deploy.ps1 --Service --Action Install --Name uploader
# 或
# powershell -command "Invoke-Command -ScriptBlock ([scriptblock]::Create($((new-object net.webclient).DownloadString('https://nexus20.tclking.com/repository/raw-hosted/tbot-devops/deploy.ps1')))) -ArgumentList $true,'Install','uploader'"
#######################################################################################

$NEXUS_URL="https://nexus.com"
$NEXUS_REPO="raw-hosted"
$NEXUS_USERNAME=${env:NEXUS_USERNAME}
$NEXUS_PASSWORD=${env:NEXUS_PASSWORD}

$NEXUS_REPO_PATH=${env:NEXUS_REPO_PATH}
$ROOT_PATH=${env:ROOT_PATH}

class DeploymentService {
    [string[]]  $DependentServices

    DeploymentService() {
        $this.DependentServices=""
    }

    # Install method
    [void] Install() {
        Write-Host "######### Entering Install function #########"
        Get-ChildItem -Path ${env:ROOT_PATH} -File  -Recurse | ForEach-Object {
            $fullName=$_.FullName
            $urlRelativePath=$fullName.Replace("${ROOT_PATH}","").Replace("\","/")
            $urlPath="${NEXUS_URL}/repository/${NEXUS_REPO}/${NEXUS_REPO_PATH}/${urlRelativePath}"
            Write-Host $fullName, $urlPath

            $params = @{
              UseBasicParsing = $true
              Uri             = $urlPath
              Method          = "PUT"
              ContentType     = "multipart/form-data"
              InFile          = $fullName
              Headers         = @{
                ContentType   = "application/json"
                Authorization = "Basic $([System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$NEXUS_USERNAME`:$NEXUS_PASSWORD")))"
              }
              Verbose         = $true
            }
            Invoke-WebRequest @params
        }
        Write-Host "######### Exiting Install function #########"
    }

    # Start method
    [void] Start() {
        Write-Host "######### Entering Start function #########"
        Write-Host "######### Exiting Start function #########"
    }

    # Status method, installed or not
    [boolean] Status() {
        Write-Host "######### Entering Status function #########"
        Write-Host "######### Exiting Status function #########"
        return $False
    }

    # Stop method
    [void] Stop() {
        Write-Host "######### Entering Stop function #########"
        Write-Host "######### Exiting Stop function #########"
    }

    # Start method
    [void] Uninstall() {
        Write-Host "######### Entering Uninstall function #########"
        Write-Host "######### Exiting Uninstall function #########"
    }
}

Function New-DeploymentService {
    return [DeploymentService]::new()
}

Export-ModuleMember -Function New-DeploymentService
