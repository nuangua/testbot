#######################################################################################
########## License:Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved           ##########
########## Author : Nuanguang Gu(Sunny)                                      ##########
########## Email  : nuanguang.gu@aliyun.com                                  ##########
#######################################################################################

class DeploymentService {
    [string[]] $DependentServices
    [string] $tbotRoot
    [string] $binPath

    DeploymentService() {
        $this.DependentServices=""

        # 从环境变量TB_HOME获取TESTBOT根目录，若不存在则设置默认目录
        $this.tbotRoot=[Environment]::GetEnvironmentVariable("TB_HOME")
        if ( -not $this.installDir ) {
            $platform=[System.Environment]::OSVersion.Platform
            if ( $platform -eq "Unix" ) {
                $this.tbotRoot="/opt/TESTBOT"
            } else {
                $this.tbotRoot="${env:HOME}/TESTBOT"
            }
        }

        # 获取$TBOT_ROOT/bin目录，作为可执行文件的存放目录
        $this.binPath=Join-Path -Path "$($this.tbotRoot)" -ChildPath "bin"
        Write-Host "binPath=$($this.binPath)"
        New-Item -ItemType Directory -Force -Path "$($this.binPath)"

    }

    # Install method
    [void] Install() {
        Write-Host "######### Entering Install function #########"

        # 查询client_main的最新版本信息，按照文件名排序
        $res=Invoke-RestMethod -Uri "https://nexus20.tclking.com/service/rest/v1/search/assets?sort=name&direction=desc&repository=raw-hosted&group=/client_main"
        $targetVersion=$null
        Foreach ($item in $res.items) {
            # Write-Host "downloadUrl="+${$item.downloadUrl}
            Write-Host "downloadUrl=$($item.downloadUrl)"
            $platform=[System.Environment]::OSVersion.Platform
            if ( $platform -eq "Unix" ) {
                if ( $($item.downloadUrl).endswith(".bin") ) {
                    $targetVersion=$item
                    break
                }
            } else {
                if ( $($item.downloadUrl).endswith(".exe") ) {
                    $targetVersion=$item
                    break
                }
            }
        }
        if ( -not $targetVersion ) {
            Write-Error "No version found!"
            exit -1
        }

        # 检查目标版本文件是否已经存在，在不存在的情况下下载文件
        $filename=$($targetVersion.path).Split("/")[1]
        $clientMainDir=Join-Path -Path "$($this.binPath)" -ChildPath "client_main"
        New-Item -ItemType Directory -Force -Path "${clientMainDir}"
        $clientMainPath=Join-Path -Path "${clientMainDir}" -ChildPath "$filename"
        if ( -not (Test-Path "$clientMainPath" -PathType Leaf) ) {
            Invoke-RestMethod -Uri "$($targetVersion.downloadUrl)" -OutFile "$clientMainPath"
            $platform=[System.Environment]::OSVersion.Platform
            if ( $platform -eq "Unix" ) {
                & chmod +x "$clientMainPath"
            }
        }

        # 创建后台服务，设置开机启动
        $servicePath=$null
        $platform=[System.Environment]::OSVersion.Platform
         if ( $platform -eq "Unix" ) {
            $servicePath="/etc/systemd/system/client_main.service"
            Set-Content -Path ${servicePath} -Value @"
[Unit]
Description=client_main Service

[Service]
WorkingDirectory=${clientMainDir}
ExecStart=${clientMainPath}
Restart=always

[Install]
WantedBy=multi-user.target
"@
             & systemctl enable client_main.service
             & systemctl start client_main.service
             & systemctl status client_main.service

            $desktopSlug="/home/admin/.config/autostart/start.desktop"
             if ( Test-Path "$desktopSlug" -PathType Leaf ) {
                Remove-Item -Path "$desktopSlug" -Force
             }
         } else {
            Write-Host "Not ready yet"
         }


        Write-Host "######### Exiting Install function #########"
    }

    # Start method
    [void] Start() {
        Write-Host "######### Entering Start function #########"
        Write-Host "######### Exiting Start function #########"
    }

    # Status method, installed or not
    [boolean] Status() {
        Write-Host "######### Entering Status function #########"
        Write-Host "######### Exiting Status function #########"
        return $False
    }

    # Stop method
    [void] Stop() {
        Write-Host "######### Entering Stop function #########"
        Write-Host "######### Exiting Stop function #########"
    }

    # Start method
    [void] Uninstall() {
        Write-Host "######### Entering Uninstall function #########"
        Write-Host "######### Exiting Uninstall function #########"
    }
}

Function New-DeploymentService {
    return [DeploymentService]::new()
}

Export-ModuleMember -Function New-DeploymentService
