# Runup client_main

## Install

```buildoutcfg
https://nexus20.tclking.com/service/rest/v1/search/assets?sort=name&direction=desc&repository=raw-hosted&group=/client_main
```

```buildoutcfg
{
    "items": [
        {
            "downloadUrl": "http://nexus20.tclking.com:8081/repository/raw-hosted/client_main/client_main-20241931102-eaf5ba4e.exe",
            "path": "client_main/client_main-20241931102-eaf5ba4e.exe",
            "id": "cmF3LWhvc3RlZDoxNTg2ZTYyZDJjZmU5ZmI4ZWY4ZDgxNjhiZDFjYjg1Nw",
            "repository": "raw-hosted",
            "format": "raw",
            "checksum": {
                "sha1": "9d5010d6f6fcca603777ab9260c8b111d0399d70",
                "md5": "432f1bf765151904ffaaaa6a8f88f57f"
            },
            "contentType": "application/x-executable",
            "lastModified": "2024-05-15T03:06:45.590+00:00"
        }
    ]
}
```

```buildoutcfg linux /etc/systemd/system/client_main.service
[Unit]
Description=client_main Service

[Service]
ExecStart=${clientMainPath}
Restart=always

[Install]
WantedBy=multi-user.target
```

```buildoutcfg windows
systemctl enable client_main.service
systemctl start client_main.service
systemctl status client_main.service
```


## Start

## Status

## Stop

## Uninstall