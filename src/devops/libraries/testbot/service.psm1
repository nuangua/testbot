#######################################################################################
########## License:Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved           ##########
########## Author : Nuanguang Gu(Sunny)                                      ##########
########## Email  : nuanguang.gu@aliyun.com                                  ##########
#######################################################################################


class DeploymentService {
    [string[]]  $DependentServices

    DeploymentService() {
        $this.DependentServices=""
    }

    # Install method
    [void] Install() {
        Write-Host "######### Entering Install function #########"
        $pythonPath = $null
        try {
            $pythonPath = (Get-Command python).Path
            & ${pythonPath} -m pip install -U pytestbot | Tee-Object -Variable output | Write-Host
        } catch {
        }
        if ($pythonPath -ne $null) {
        } else {
            Write-Host "No python found! Please make sure python is installed."
        }

        Write-Host "######### Exiting Install function #########"
    }

    # Start method
    [void] Start() {
        Write-Host "######### Entering Start function #########"
        Write-Host "######### Exiting Start function #########"
    }

    # Status method, installed or not
    [boolean] Status() {
        Write-Host "######### Entering Status function #########"
        Write-Host "######### Exiting Status function #########"
        return $False
    }

    # Stop method
    [void] Stop() {
        Write-Host "######### Entering Stop function #########"
        Write-Host "######### Exiting Stop function #########"
    }

    # Start method
    [void] Uninstall() {
        Write-Host "######### Entering Uninstall function #########"
        Write-Host "######### Exiting Uninstall function #########"
    }
}

Function New-DeploymentService {
    return [DeploymentService]::new()
}

Export-ModuleMember -Function New-DeploymentService
