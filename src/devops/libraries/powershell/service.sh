#!/bin/bash

# install powershell core

if [[ ! -x "$(command -v pwsh)" ]]; then
        echo 'pwsh is not installed.'
        install_dir="/opt/powershell"
        download_filename="powershell-7.4.2-linux-arm64.tar.gz"
        curl -LO /opt/powershell-7.4.2-linux-arm64.tar.gz https://nexus20.tclking.com/repository/raw-hosted/tbot-devops/libraries/powershell/powershell-7.4.2-linux-arm64.tar.gz
        if [[ ! -e "$install_dir" ]]; then
                mkdir -p /opt/powershell
        elif [[ ! -d "$install_dir" ]]; then
                echo "$install_dir already exists but is not a directory!"
        fi

        if [[ ! -f "/opt/powershell/pwsh" ]]; then
                tar zxvf /opt/powershell-7.4.2-linux-arm64.tar.gz -C /opt/powershell
                chmod +x -R /opt/powershell
        fi
        ln -s /opt/powershell /usr/bin/pwsh

fi

pwsh -V
