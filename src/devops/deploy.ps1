#######################################################################################
########## License:Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved           ##########
########## Author : Nuanguang Gu(Sunny)                                      ##########
########## Email  : nuanguang.gu@aliyun.com                                  ##########
#######################################################################################


param(
 [string]$Action,
 [string]$Name
)

class ServiceManager {
    [string]$Action
    [string]$Name

    ServiceManager([string]$action, [string]$name) {
        $this.Action=$action
        $this.Name=$name
    }

    [System.Management.Automation.PSModuleInfo] ImportService() {
        Write-Host "######### Entering Import-Service-Module function #########"
        $_name=$this.Name
        $mod=New-Module -Name "service" -ScriptBlock ([Scriptblock]::Create((New-Object System.Net.WebClient).DownloadString("https://gitlab.com/nuangua/testbot/-/raw/main/src/devops/libraries/$_name/service.psm1")))
        Import-Module $mod
        Write-Host "######### Exiting Import-Service-Module function #########"
        return $mod
    }

    [void] RemoveService([System.Management.Automation.PSModuleInfo]$mod) {
        Write-Host "######### Entering Remove-Service-Module function #########"
        Remove-Module $mod
        Write-Host "######### Exiting Remove-Service-Module function #########"
    }

    [void] Run() {
        $_action=$this.Action
        try {
            $serviceInst=New-DeploymentService
            switch ($_action) {
                "Install" {
                    $serviceInst.Install()
                }
                "Start" {
                    $serviceInst.Start()
                }
                "Stop" {
                    $serviceInst.Stop()
                }
                "Uninstall" {
                    $serviceInst.Uninstall()
                }
            }
        }
        catch {
          Write-Error "An error occurred when calling $_action function:"
          Write-Host $_
        }

    }

}

function Handle-Parameters() {
    Write-Host "Action=$Action"
    Write-Host "Name=$Name"
}

function Prepare-Envs() {
}

function Main() {
    Handle-Parameters
    Prepare-Envs

    try {
        $sm=[ServiceManager]::new($Action, $Name)
        $mod=$sm.ImportService()
        $sm.Run()
        $sm.RemoveService($mod)
    }
    catch {
      Write-Error "An error occurred:"
      Write-Host $_
    }

}


Main
