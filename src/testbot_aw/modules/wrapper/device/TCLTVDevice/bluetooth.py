#!/usr/bin/env python
# -*- coding: utf-8 -*-

__copyright__ = "Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved"
__author__ = "Nuanguang Gu(Sunny)"
__email__ = "nuanguang.gu@aliyun.com"

from testbot.resource.module import TCLTVDeviceWrapperModuleBase


class BluetoothWrapperModule(TCLTVDeviceWrapperModuleBase):
    """
    TCLTV测试设备源封装接口蓝牙模块类
    """
    pass
