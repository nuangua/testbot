#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
TCLTVDevice设备原子接口模块类列表
"""

__copyright__ = "Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved"
__author__ = "Nuanguang Gu(Sunny)"
__email__ = "nuanguang.gu@aliyun.com"

from testbot.resource.module import TCLTVDeviceAtomModuleBase


class ADBAtomModule(TCLTVDeviceAtomModuleBase):
    """
    TCLTV测试设备源原子接口ADB连接模块类
    """
    pass
