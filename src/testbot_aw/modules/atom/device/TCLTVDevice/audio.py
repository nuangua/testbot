#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
TCLTVDevice设备原子接口模块类列表
"""

__copyright__ = "Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved"
__author__ = "Nuanguang Gu(Sunny)"
__email__ = "nuanguang.gu@aliyun.com"

from testbot.resource.module import TCLTVDeviceAtomModuleBase


class AudioAtomModule(TCLTVDeviceAtomModuleBase):
    """
    TCLTV测试设备源原子接口音频模块类
    """
    pass
