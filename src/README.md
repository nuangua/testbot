# 启动web服务

```buildoutcfg
python -m testbot_webui.manage migrate

python -m testbot_webui.manage runserver
```

# 检查环境

```buildoutcfg
python -m testbot.cli env inspect --type SMOKE_TEST --type SYSTEM_TEST
```

# 执行测试

```buildoutcfg
python -m testbot.cli runner test --testlist testbot/example/TCList.json --resource testbot/example/ResourcePool.json
```
