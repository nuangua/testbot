# 执行用例

## 卸载老版本

```buildoutcfg
python -m pip uninstall -y testbot
```

## 安装testbot

```buildoutcfg
python -m pip install -U testbot
```

## 检查安装情况

```buildoutcfg
python -m pip show testbot
```
## 执行脚本

```buildoutcfg
python -m testbot.cli runner test --testlist src/testbot_apps/scripts/unit_test/TCList.json --resource src/testbot_apps/scripts/unit_test/pool.json
```
