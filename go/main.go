package main

import (
	"fmt"
	"gitlab.com/nuangua/testbot/resource"
	"gitlab.com/nuangua/testbot/resource/device"
)

func main() {
	var tv = device.NewTCLTVDevice("TV1", "TVDevice")
	var grpcPort = resource.NewPort("10.120.26.46:60000", "GRPCPort", &tv.Device)
	tv.Ports[grpcPort.Name] = grpcPort
	fmt.Println(tv)
	alive, err := tv.CheckHeartBeat()
	fmt.Println("心跳保活状态：", alive)
	fmt.Println(err)
}
