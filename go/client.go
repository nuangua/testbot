package main

import (
	"context"
	"log"

	"gitlab.com/nuangua/testbot/resource/protoc"

	"google.golang.org/grpc"
)

func main() {
	// conn, err := grpc.Dial("10.120.26.46:60000", grpc.WithInsecure())
	conn, err := grpc.Dial("127.0.0.1:60000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := protoc.NewHeartBeatClient(conn)
	resp, err := client.CheckHeartBeat(context.Background(), &protoc.Empty{})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Println("receive message:", resp)
}
