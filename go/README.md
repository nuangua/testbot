# 安装protocol buffers

下载路径：https://github.com/protocolbuffers/protobuf/releases/tag/v3.19.4

* 手动下载protobuffer，解压并拷贝到GOPATH的bin目录
* 将include目录的文件夹拷贝到项目根目录 `C:\Program Files\Go\src`

# 获取grpc核心库和protoc-gen-go-grpc

```aiignore
go install google.golang.org/grpc
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

# 替换testbot本地库

```aiignore
go mod edit -replace gitlab.com/nuangua/testbot=./testbot
```

# 编译proto文件

```aiignore
protoc --go_out=%cd%/ --go_opt=paths=source_relative --go-grpc_out=%cd%/ --go-grpc_opt=paths=source_relative %cd%/testbot/resource/protoc/*.proto -I%cd%/

protoc --go_out=$(pwd)/ --go_opt=paths=source_relative --go-grpc_out=$(pwd)/ --go-grpc_opt=paths=source_relative $(pwd)/testbot/resource/protoc/*.proto -I$(pwd)/
```

# 编译可执行程序

## Windows平台

```aiignore
go env -w CGO_ENABLED=0 GOOS=windows GOARCH=amd64
go build -o testbot-agent.exe main.go
```
## Android平台

```aiignore
go env -w CGO_ENABLED=0 GOOS=linux GOARCH=arm64
go build -o testbot-agent.bin main.go
```

## macOS平台

```commandline
go env -w CGO_ENABLED=0 GOOS=darwin GOARCH=amd64
go build -o testbot-agent.app main.go
```
