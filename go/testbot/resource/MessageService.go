package resource

import (
	"fmt"

	"gitlab.com/nuangua/testbot/resource/protoc"
	"golang.org/x/net/context"
)

type MessageService struct {
}

func (m MessageService) SayHello(c context.Context, request *protoc.HelloRequest) (*protoc.HelloReply, error) {
	return &protoc.HelloReply{Message: "Server say hello to " + request.GetName()}, nil
}

func (m MessageService) mustEmbedUnimplementedGreeterServer() {
	//TODO implement me
	fmt.Println("implement me")
}
