package device

import "gitlab.com/nuangua/testbot/resource"

type PCDevice struct {
	resource.Device
}

type AndroidDevice struct {
	resource.Device
}

type TVDevice struct {
	AndroidDevice
}

type PhoneDevice struct {
	AndroidDevice
}

type TabletDevice struct {
	AndroidDevice
}

type MonitorDevice struct {
	AndroidDevice
}
