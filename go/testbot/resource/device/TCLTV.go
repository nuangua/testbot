package device

import (
	"context"
	"google.golang.org/grpc"
	"gitlab.com/nuangua/testbot/resource"
	"gitlab.com/nuangua/testbot/resource/protoc"
)

type TCLTVDevice struct {
	TVDevice
}

func NewTCLTVDevice(name string, _type string) *TCLTVDevice {
	return &TCLTVDevice{
		TVDevice: TVDevice{
			AndroidDevice: AndroidDevice{
				Device: resource.Device{
					Resource: resource.Resource{
						Name: name,
						Type: _type,
						MODULES: []string{
							// 封装接口
							"tcl.com/tatf/tatf_aw/modules/wrapper/device/TCLTVDevice/commserial/CommSerialWrapperModule",
							// 基础原子接口
							"tcl.com/tatf/tatf_aw/modules/atom/device/TCLTVDevice/commserial/CommSerialAtomModule",
						},
						Ports: make(map[string]*resource.Port),
					},
				},
			},
		},
	}
}

func (device *TCLTVDevice) GetPort(_type string) *resource.Port {
	var port = &resource.Port{}
	for _, _port := range device.Ports {
		if _port.Type == _type {
			port = _port
		}
	}
	return port
}

func (device *TCLTVDevice) CheckHeartBeat() (string, error) {
	port := device.GetPort("GRPPort")
	conn, err := grpc.Dial(port.Name, grpc.WithInsecure())
	// conn, err := grpc.Dial("127.0.0.1:60000", grpc.WithInsecure())
	if err != nil {
		return "", err
	}
	defer conn.Close()

	client := protoc.NewHeartBeatClient(conn)
	resp, err := client.CheckHeartBeat(context.Background(), &protoc.Empty{})
	if err != nil {
		return "", err
	}
	return resp.GetValue(), nil
}
