// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.3
// 	protoc        v3.19.4
// source: testbot/resource/protoc/basic_type.proto

package protoc

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 封装 int 类型
type IntType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         int32                  `protobuf:"varint,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *IntType) Reset() {
	*x = IntType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *IntType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IntType) ProtoMessage() {}

func (x *IntType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use IntType.ProtoReflect.Descriptor instead.
func (*IntType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{0}
}

func (x *IntType) GetValue() int32 {
	if x != nil {
		return x.Value
	}
	return 0
}

// 封装 long 类型
type LongType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         int64                  `protobuf:"varint,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *LongType) Reset() {
	*x = LongType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[1]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *LongType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LongType) ProtoMessage() {}

func (x *LongType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[1]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LongType.ProtoReflect.Descriptor instead.
func (*LongType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{1}
}

func (x *LongType) GetValue() int64 {
	if x != nil {
		return x.Value
	}
	return 0
}

// 封装 float 类型
type FloatType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         float32                `protobuf:"fixed32,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *FloatType) Reset() {
	*x = FloatType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[2]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *FloatType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FloatType) ProtoMessage() {}

func (x *FloatType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[2]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FloatType.ProtoReflect.Descriptor instead.
func (*FloatType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{2}
}

func (x *FloatType) GetValue() float32 {
	if x != nil {
		return x.Value
	}
	return 0
}

// 封装 double 类型
type DoubleType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         float64                `protobuf:"fixed64,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *DoubleType) Reset() {
	*x = DoubleType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[3]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *DoubleType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DoubleType) ProtoMessage() {}

func (x *DoubleType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[3]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DoubleType.ProtoReflect.Descriptor instead.
func (*DoubleType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{3}
}

func (x *DoubleType) GetValue() float64 {
	if x != nil {
		return x.Value
	}
	return 0
}

// 封装 char 类型（使用 string，因为 char 不是 proto3 的原生类型）
type CharType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         string                 `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *CharType) Reset() {
	*x = CharType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[4]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *CharType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CharType) ProtoMessage() {}

func (x *CharType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[4]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CharType.ProtoReflect.Descriptor instead.
func (*CharType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{4}
}

func (x *CharType) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

// 封装 string 类型
type StringType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         string                 `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *StringType) Reset() {
	*x = StringType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[5]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *StringType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*StringType) ProtoMessage() {}

func (x *StringType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[5]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use StringType.ProtoReflect.Descriptor instead.
func (*StringType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{5}
}

func (x *StringType) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

// 封装 boolean 类型
type BooleanType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         bool                   `protobuf:"varint,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *BooleanType) Reset() {
	*x = BooleanType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[6]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *BooleanType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BooleanType) ProtoMessage() {}

func (x *BooleanType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[6]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BooleanType.ProtoReflect.Descriptor instead.
func (*BooleanType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{6}
}

func (x *BooleanType) GetValue() bool {
	if x != nil {
		return x.Value
	}
	return false
}

// 封装 byte 类型（使用 bytes 来封装）
type ByteType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         []byte                 `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *ByteType) Reset() {
	*x = ByteType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[7]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *ByteType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ByteType) ProtoMessage() {}

func (x *ByteType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[7]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ByteType.ProtoReflect.Descriptor instead.
func (*ByteType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{7}
}

func (x *ByteType) GetValue() []byte {
	if x != nil {
		return x.Value
	}
	return nil
}

// 封装 void 类型
type VoidType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Void          *emptypb.Empty         `protobuf:"bytes,1,opt,name=void,proto3" json:"void,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *VoidType) Reset() {
	*x = VoidType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[8]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *VoidType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*VoidType) ProtoMessage() {}

func (x *VoidType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[8]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use VoidType.ProtoReflect.Descriptor instead.
func (*VoidType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{8}
}

func (x *VoidType) GetVoid() *emptypb.Empty {
	if x != nil {
		return x.Void
	}
	return nil
}

// 空消息类型
type Empty struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *Empty) Reset() {
	*x = Empty{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[9]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *Empty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Empty) ProtoMessage() {}

func (x *Empty) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[9]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Empty.ProtoReflect.Descriptor instead.
func (*Empty) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{9}
}

// 封装 short 类型（使用 int32）
type ShortType struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Value         int32                  `protobuf:"varint,1,opt,name=value,proto3" json:"value,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *ShortType) Reset() {
	*x = ShortType{}
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[10]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *ShortType) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ShortType) ProtoMessage() {}

func (x *ShortType) ProtoReflect() protoreflect.Message {
	mi := &file_testbot_resource_protoc_basic_type_proto_msgTypes[10]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ShortType.ProtoReflect.Descriptor instead.
func (*ShortType) Descriptor() ([]byte, []int) {
	return file_testbot_resource_protoc_basic_type_proto_rawDescGZIP(), []int{10}
}

func (x *ShortType) GetValue() int32 {
	if x != nil {
		return x.Value
	}
	return 0
}

var File_testbot_resource_protoc_basic_type_proto protoreflect.FileDescriptor

var file_testbot_resource_protoc_basic_type_proto_rawDesc = []byte{
	0x0a, 0x28, 0x74, 0x65, 0x73, 0x74, 0x62, 0x6f, 0x74, 0x2f, 0x72, 0x65, 0x73, 0x6f, 0x75, 0x72,
	0x63, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x2f, 0x62, 0x61, 0x73, 0x69, 0x63, 0x5f,
	0x74, 0x79, 0x70, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x17, 0x74, 0x65, 0x73, 0x74,
	0x62, 0x6f, 0x74, 0x2e, 0x72, 0x65, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x63, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x22, 0x1f, 0x0a, 0x07, 0x49, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76,
	0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75,
	0x65, 0x22, 0x20, 0x0a, 0x08, 0x4c, 0x6f, 0x6e, 0x67, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x22, 0x21, 0x0a, 0x09, 0x46, 0x6c, 0x6f, 0x61, 0x74, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x02, 0x52,
	0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x22, 0x0a, 0x0a, 0x44, 0x6f, 0x75, 0x62, 0x6c, 0x65,
	0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x01, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x20, 0x0a, 0x08, 0x43, 0x68,
	0x61, 0x72, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x22, 0x0a, 0x0a,
	0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61,
	0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65,
	0x22, 0x23, 0x0a, 0x0b, 0x42, 0x6f, 0x6f, 0x6c, 0x65, 0x61, 0x6e, 0x54, 0x79, 0x70, 0x65, 0x12,
	0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x20, 0x0a, 0x08, 0x42, 0x79, 0x74, 0x65, 0x54, 0x79, 0x70,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0c,
	0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x22, 0x36, 0x0a, 0x08, 0x56, 0x6f, 0x69, 0x64, 0x54,
	0x79, 0x70, 0x65, 0x12, 0x2a, 0x0a, 0x04, 0x76, 0x6f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x52, 0x04, 0x76, 0x6f, 0x69, 0x64, 0x22,
	0x07, 0x0a, 0x05, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x21, 0x0a, 0x09, 0x53, 0x68, 0x6f, 0x72,
	0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x05, 0x52, 0x05, 0x76, 0x61, 0x6c, 0x75, 0x65, 0x42, 0x0a, 0x5a, 0x08, 0x2e,
	0x3b, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_testbot_resource_protoc_basic_type_proto_rawDescOnce sync.Once
	file_testbot_resource_protoc_basic_type_proto_rawDescData = file_testbot_resource_protoc_basic_type_proto_rawDesc
)

func file_testbot_resource_protoc_basic_type_proto_rawDescGZIP() []byte {
	file_testbot_resource_protoc_basic_type_proto_rawDescOnce.Do(func() {
		file_testbot_resource_protoc_basic_type_proto_rawDescData = protoimpl.X.CompressGZIP(file_testbot_resource_protoc_basic_type_proto_rawDescData)
	})
	return file_testbot_resource_protoc_basic_type_proto_rawDescData
}

var file_testbot_resource_protoc_basic_type_proto_msgTypes = make([]protoimpl.MessageInfo, 11)
var file_testbot_resource_protoc_basic_type_proto_goTypes = []any{
	(*IntType)(nil),       // 0: testbot.resource.protoc.IntType
	(*LongType)(nil),      // 1: testbot.resource.protoc.LongType
	(*FloatType)(nil),     // 2: testbot.resource.protoc.FloatType
	(*DoubleType)(nil),    // 3: testbot.resource.protoc.DoubleType
	(*CharType)(nil),      // 4: testbot.resource.protoc.CharType
	(*StringType)(nil),    // 5: testbot.resource.protoc.StringType
	(*BooleanType)(nil),   // 6: testbot.resource.protoc.BooleanType
	(*ByteType)(nil),      // 7: testbot.resource.protoc.ByteType
	(*VoidType)(nil),      // 8: testbot.resource.protoc.VoidType
	(*Empty)(nil),         // 9: testbot.resource.protoc.Empty
	(*ShortType)(nil),     // 10: testbot.resource.protoc.ShortType
	(*emptypb.Empty)(nil), // 11: google.protobuf.Empty
}
var file_testbot_resource_protoc_basic_type_proto_depIdxs = []int32{
	11, // 0: testbot.resource.protoc.VoidType.void:type_name -> google.protobuf.Empty
	1,  // [1:1] is the sub-list for method output_type
	1,  // [1:1] is the sub-list for method input_type
	1,  // [1:1] is the sub-list for extension type_name
	1,  // [1:1] is the sub-list for extension extendee
	0,  // [0:1] is the sub-list for field type_name
}

func init() { file_testbot_resource_protoc_basic_type_proto_init() }
func file_testbot_resource_protoc_basic_type_proto_init() {
	if File_testbot_resource_protoc_basic_type_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_testbot_resource_protoc_basic_type_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   11,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_testbot_resource_protoc_basic_type_proto_goTypes,
		DependencyIndexes: file_testbot_resource_protoc_basic_type_proto_depIdxs,
		MessageInfos:      file_testbot_resource_protoc_basic_type_proto_msgTypes,
	}.Build()
	File_testbot_resource_protoc_basic_type_proto = out.File
	file_testbot_resource_protoc_basic_type_proto_rawDesc = nil
	file_testbot_resource_protoc_basic_type_proto_goTypes = nil
	file_testbot_resource_protoc_basic_type_proto_depIdxs = nil
}
