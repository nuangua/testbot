package resource

type Serializable interface {
	ToMap() map[string]interface{}
	FromMap(data map[string]interface{}) interface{}
}
type Resource struct {
	Name        string
	Type        string
	Description string
	MODULES     []string
	Ports       map[string]*Port
	ClientAttrs map[string]interface{}
	SharedAttrs map[string]interface{}
	ServerAttrs map[string]interface{}
}

func (r *Resource) ToMap() map[string]interface{} {
	return make(map[string]interface{})
}

func (r *Resource) FromMap(data map[string]interface{}) {
}

func (r *Resource) InitResource(testType string, reload bool) {}

func (r *Resource) CleanResource() {}

type Device struct {
	Resource
}

type Port struct {
	Resource
	instance    interface{}
	Parent      *Device
	RemotePorts []*Port
}

func NewPort(name string, _type string, parent *Device) *Port {
	return &Port{
		Resource: Resource{
			Name:        name,
			Type:        _type,
			Description: "",
		},
		Parent: parent,
	}
}

type Software struct {
	Resource
}

type Service struct {
	Resource
}
