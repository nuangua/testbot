# 测试用例脚本示例集

# 测试用例规范

* 一条完整的测试用例信息，必须包含：测试用例ID、测试用例名称、一个或多条前置条件、一个或多条测试步骤、一个或多条期望结果
* 测试步骤和期望结果通过序号相关联，即一条测试步骤执行后，检查与之相关联的期望结果

## 示例1
### 输入测试用例信息

```buildoutcfg
测试用例ID：SQAST-12685
测试用例名称：HotWordMicContentProvider 与FarFieldMicContentProvider 两个语音指示灯数据库存在
前置条件：
1. 已升级系统版本；
2. PC连接TV
测试步骤：
1. 执行adb命令 dumpsys package com.tcl.systemserver | grep Provider
2. 查看 HotWordMicContentProvider、FarFieldMicContentProvider 两个数据库是否存在
期望结果：
2. 可以看到 HotWordMicContentProvider、FarFieldMicContentProvider
```

### 输出测试脚本代码

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
测试用例ID：SQAST-12685
测试用例名称：HotWordMicContentProvider 与FarFieldMicContentProvider 两个语音指示灯数据库存在
前置条件：
1. 已升级系统版本；
2. PC连接TV
测试步骤：
1. 执行adb命令 dumpsys package com.tcl.systemserver | grep Provider
2. 查看 HotWordMicContentProvider、FarFieldMicContentProvider 两个数据库是否存在
期望结果：
2. 可以看到 HotWordMicContentProvider、FarFieldMicContentProvider
"""

__copyright__ = "Copyright (c) 2024 Nuanguang Gu(Sunny) Reserved"
__author__ = "Nuanguang Gu(Sunny)"
__email__ = "nuanguang.gu@aliyun.com"

from testbot.app.case.base import TestCaseBase
from testbot.app.base import TestType
from testbot.app.case.decorator import case

SERIAL_COMMAND = "dumpsys package com.tcl.systemserver | grep Provider"
HOT_WORD_MIC_CONTENT_PROVIDER = "HotWordMicContentProvider"
FAR_FIELD_MIC_CONTENT_PROVIDER = "FarFieldMicContentProvider"

@case(test_type=TestType.HWCOMB_COMM_TEST.name, testcase_id="SQAST-12685", testcase_name="HotWordMicContentProvider 与FarFieldMicContentProvider 两个语音指示灯数据库存在")
class Template(TestCaseBase):

    def collect_resource(self, **kwargs):
        with self.step.start(headline="筛选设备") as step:
            with step.start(headline="筛选PC设备") as step2:
                self.pc = self.pool.collect_device(device_type="PCDevice", count=1)[0]
                if self.pc:
                    self.logger.info(f"self.pc={self.pc}, type={type(self.pc)}")
                    self.logger.info(f"self.pc funcs={dir(self.pc)}")
                    step2.passed(message="筛选PC设备成功")
                else:
                    step2.failed(message="筛选PC设备失败")
            with step.start(headline="筛选TV设备") as step2:
                self.tv = self.pool.collect_device(device_type="TVDevice", count=1)[0]
                if self.tv:
                    self.logger.info(f"self.tv={self.tv}, type={type(self.tv)}")
                    self.logger.info(f"self.tv funcs={dir(self.tv)}")
                    step2.passed(message="筛选TV设备成功")
                else:
                    step2.failed(message="筛选TV设备失败")
        pass
    
    def setup(self, **kwargs):
        with self.step.start(headline="已升级系统版本") as step:
            pass
        with self.step.start(headline="PC连接TV") as step2:
            pass
    
    def test(self, **kwargs):
        with self.step.start(headline="执行adb命令 dumpsys package com.tcl.systemserver | grep Provider") as step:
            output = self.tv.CommSerialAtomModule.send_command(data=SERIAL_COMMAND)
            if output == "":
                step.failed(message=SERIAL_COMMAND + "执行返回为空")
            with self.step.start(headline="查看 HotWordMicContentProvider、FarFieldMicContentProvider 两个数据库是否存在") as step:
                if HOT_WORD_MIC_CONTENT_PROVIDER in output and FAR_FIELD_MIC_CONTENT_PROVIDER in output:
                    step.passed(message=SERIAL_COMMAND + " 执行,发现 " + HOT_WORD_MIC_CONTENT_PROVIDER + " 与 " + FAR_FIELD_MIC_CONTENT_PROVIDER + " ,执行后的回显为：\n" + output)
                else:
                    step.failed(message=SERIAL_COMMAND + " 执行,未发现 " + HOT_WORD_MIC_CONTENT_PROVIDER + " 与 " + FAR_FIELD_MIC_CONTENT_PROVIDER + " ,执行后的回显为：\n" + output)
    
    def cleanup(self, **kwargs):
        pass
```
