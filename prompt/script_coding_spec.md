# 测试脚本编码规范

* 一条测试脚本，对应一条继承了测试用例基类testbot.app.case.base.TestCaseBase的测试用例类
* 所有测试用例类，必须统一继承测试框架提供的测试用例基类testbot.app.case.base.TestCaseBase

```python
from testbot.app.case.base import TestCaseBase
 
class Test(TestCaseBase):
    ...
```

* 所有测试用例类必须通过装饰器方法testbot.app.case.decorator.case，指定测试参数如优先级priority、测试类型test_type、测试用例ID testcase_id、测试用例名称testcase_name

```python
from testbot.app.case.base import TestCaseBase
from testbot.app.case.decorator import case
from testbot.app.base import TestType
 
@case(priority=1, test_type=TestType.CHECK_TEST.name, testcase_id="CNTC-94402", testcase_name="恢复出厂设置")
class Test(TestCaseBase):
    ...
```

* 测试类型test_type可选参数有下面这些

```python
    # 单元测试
    UNIT_TEST = 0b00000000
    # 沙盒测试
    SANITY_TEST = 0b00010000
    # 集成测试
    INTEGRATION_TEST = 0b00100000
    # 冒烟测试
    SMOKE_TEST = 0b00110000
    # 系统冒烟测试
    SMOKE_TEST__SYSTEM = 0b00110001
    # 中间件冒烟测试
    SMOKE_TEST__MIDDLEWARE = 0b00110010
    # 系统测试
    SYSTEM_TEST = 0b01000000
    # 稳定性测试
    STABILITY_TEST = 0b01010000
    # 性能测试
    PERFORMANCE_TEST = 0b01100000
    # 点检测试
    CHECK_TEST = 0b01110000
    # 接口测试
    INTERFACE_TEST = 0b10000000
    # 专项测试
    SPECIAL_TEST = 0b10000000
    # 媒资专项测试
    SPECIAL_TEST__MEDIA = 0b10000001

    # 硬件组合类型，未连接任何端口
    HWCOMB_TEST = 0b10010000
    # 硬件组合类型，连接端口：指令串口
    HWCOMB_COMM_TEST = 0b10010001
    # 硬件组合类型，连接端口：指令串口、红外串口
    HWCOMB_COMM_INFRA_TEST = 0b10010010
    # 硬件组合类型，连接端口：指令串口、红外串口、采集卡串口
    HWCOMB_COMM_INFRA_CAP_TEST = 0b10010011
    # 硬件组合类型，连接端口：指令串口、红外串口、采集卡串口、ADB无线连接
    HWCOMB_COMM_INFRA_CAP_ADB_TEST = 0b10010100
    # 硬件组合类型，连接端口：指令串口、红外串口、采集卡串口、ADB无线连接、GRPC连接
    HWCOMB_COMM_INFRA_CAP_ADB_GRPC_TEST = 0b10010101
    # 硬件组合类型，连接端口：指令串口、红外串口、采集卡串口、ADB无线连接、GRPC连接、音频输入口
    HWCOMB_COMM_INFRA_CAP_ADB_GRPC_AUDIO_TEST = 0b10010110
    # 硬件组合类型，连接端口：指令串口、红外串口、采集卡串口、ADB无线连接、GRPC连接、音频输入口、电源通断控制口
    HWCOMB_COMM_INFRA_CAP_ADB_GRPC_AUDIO_POWER_TEST = 0b10010111
    # 硬件组合类型，连接端口：采集卡串口、ADB无线连接、音频输入口
    HWCOMB_CAP_ADB_AUDIO_TEST = 0b10011000
    # 硬件组合类型，连接端口：指令串口、GRPC连接
    HWCOMB_COMM_GRPC_TEST = 0b10011001
    # 硬件组合类型，连接端口：指令串口、采集卡串口
    HWCOMB_COMM_CAP_TEST = 0b10011010
    # 硬件组合类型，连接端口：指令串口、Murideo设备websocket端口
    HWCOMB_COMM_MWS_TEST = 0b10011011
    # 硬件组合类型，所有连接端口
    HWCOMB_ALL_TEST = 0b10011111

    # 通用测试
    COMMON_TEST = 0b11111111
```

* 所有测试用例类必须覆盖并实现以下方法：初始化测试资源对象方法collect_resource、测试的每个循环执行之前的初始化方法setup、每个循环的测试执行体方法test、测试的每个循环执行之后的清理方法cleanup四个抽象方法

```python
from testbot.app.case.base import TestCaseBase
from testbot.app.case.decorator import case
from testbot.app.base import TestType
 
@case(priority=1, test_type=TestType.CHECK_TEST.name, testcase_id="CNTC-94402", testcase_name="恢复出厂设置")
class Test(TestCaseBase):
   def collect_resource(self, **kwargs):
        pass
 
    def setup(self, **kwargs):
        pass
 
    def test(self, **kwargs):
        pass
 
    def cleanup(self, **kwargs):
        pass
```

* 所有测试用例类可选择覆盖实现以下方法：测试执行之前的初始化方法setup_class、测试执行之后的清理方法cleanup_class方法

```python
from testbot.app.case.base import TestCaseBase
from testbot.app.case.decorator import case
from testbot.app.base import TestType
 
@case(priority=1, test_type=TestType.CHECK_TEST.name, testcase_id="CNTC-94402", testcase_name="恢复出厂设置")
class Test(TestCaseBase):
    def setup_class(self, **kwargs):
        pass
    
   def collect_resource(self, **kwargs):
        pass
 
    def setup(self, **kwargs):
        pass
 
    def test(self, **kwargs):
        pass
 
    def cleanup(self, **kwargs):
        pass
    
    def cleanup_class(self, **kwargs):
        pass
```

* 用例方法里可以通过self.logger.<LEVEL>打印不同级别的日志信息

```python
from testbot.app.case.base import TestCaseBase
from testbot.app.case.decorator import case
from testbot.app.base import TestType
 
@case(priority=1, test_type=TestType.CHECK_TEST.name, testcase_id="CNTC-94402", testcase_name="恢复出厂设置")
class Test(TestCaseBase):
    def setup_class(self, **kwargs):
        self.logger.debug(msg="打印DEBUG级别的日志信息")
        self.logger.info(msg="打印INFO级别的日志信息")
        self.logger.warn(msg="打印WARN级别的日志信息")
        self.logger.error(msg="打印ERROR级别的日志信息")
        self.logger.critial(msg="打印CRITIAL级别的日志信息")
    
   def collect_resource(self, **kwargs):
        pass
 
    def setup(self, **kwargs):
        pass
 
    def test(self, **kwargs):
        pass
 
    def cleanup(self, **kwargs):
        pass
    
    def cleanup_class(self, **kwargs):
        pass
```

* 用例方法里可以通过self.step.start()创建一个新的验证点对象，验证点节点对象可创建多个子验证点节点对象
* 验证点可以相互嵌套，构建验证点层级树
* 每个验证点对象提供passed/failed/errored等断言方法

```python
from testbot.app.case.base import TestCaseBase
from testbot.app.case.decorator import case
from testbot.app.base import TestType
 
@case(priority=1, test_type=TestType.CHECK_TEST.name, testcase_id="CNTC-94402", testcase_name="恢复出厂设置")
class Test(TestCaseBase):
    def setup_class(self, **kwargs):
        self.logger.debug(msg="打印DEBUG级别的日志信息")
        self.logger.info(msg="打印INFO级别的日志信息")
        self.logger.warn(msg="打印WARN级别的日志信息")
        self.logger.error(msg="打印ERROR级别的日志信息")
        self.logger.critial(msg="打印CRITIAL级别的日志信息")
    
   def collect_resource(self, **kwargs):
        with self.step.start(headline="筛选设备") as step:
            with step.start(headline="筛选PC设备") as step2:
                self.pc = self.pool.collect_device(device_type="PCDevice", count=1)[0]
                if self.pc:
                    self.logger.info(f"self.pc={self.pc}, type={type(self.pc)}")
                    self.logger.info(f"self.pc funcs={dir(self.pc)}")
                    step2.passed(message="筛选PC设备成功")
                else:
                    step2.failed(message="筛选PC设备失败")
            with step.start(headline="筛选TV设备") as step2:
                self.tv = self.pool.collect_device(device_type="TVDevice", count=1)[0]
                if self.tv:
                    self.logger.info(f"self.tv={self.tv}, type={type(self.tv)}")
                    self.logger.info(f"self.tv funcs={dir(self.tv)}")
                    step2.passed(message="筛选TV设备成功")
                else:
                    step2.failed(message="筛选TV设备失败")
 
    def setup(self, **kwargs):
        pass
 
    def test(self, **kwargs):
        pass
 
    def cleanup(self, **kwargs):
        pass
    
    def cleanup_class(self, **kwargs):
        pass
```
