================================
全部API接口
================================

.. raw:: html

    <div id="json-data-table"></div>
    <script>
        function jsonToTable(jsonData) {
            var table = document.createElement('table');
            var thead = document.createElement('thead');
            var tbody = document.createElement('tbody');
            var headers = Object.keys(jsonData[0]);

            // 创建表头
            var theadTr = document.createElement('tr');
            headers.forEach(function(header) {
                var th = document.createElement('th');
                th.textContent = header;
                theadTr.appendChild(th);
            });
            thead.appendChild(theadTr);
            table.appendChild(thead);

            // 创建表数据行
            jsonData.forEach(function(row) {
                var tr = document.createElement('tr');
                headers.forEach(function(header) {
                    var td = document.createElement('td');
                    td.textContent = row[header];
                    tr.appendChild(td);
                });
                tbody.appendChild(tr);
            });
            table.appendChild(tbody);

            return table;
        }

        // 读取JSON数据并渲染表格
        fetch('apis.json')
            .then(response => response.json())
            .then(data => document.getElementById('json-data-table').appendChild(jsonToTable(data)));
    </script>
